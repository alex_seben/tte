/* ********************* */
/*      Main Script      */
/* ********************* */

function validaEmail(email)
{
	var filter = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
	if( !filter.test(email) )
		return false;
	else
		return true;
};
 
$(document).ready(function() {
    
	$(".btn-ver-mais-detalhes").click(
            function(){
                $(this).next(".detalhes-vaga").slideToggle();
                if($(this).next(".detalhes-vaga").is(":visible")){   
                    $(this).html('ocultar detalhes');
                }else{
                    $(this).html('ver mais detalhes');
                }         
            }
        );
        
        $("#btn-fazer-login").click(
            function(){
                $("#formulario-login").load("formulario-login.html");            
            }
        );
        
        $("#btn-enviar-contato").click(
            function( event ){
                
                event.preventDefault();  <!-- prevenir de enviar antes de validar -->
                        
                var temErro = false;
                
                <!-- VALIDAR NOME -->
                if( $.trim( $("#txtNome").val() ) == "" ){      <!-- trim(pra evitar espaço ser validado )-->                          
                    $("#divTxtNome").removeClass("has-success").addClass("has-error");
                    temErro = true;
                }else{
                    $("#divTxtNome").removeClass("has-error").addClass("has-success");
                }
                
                <!-- VALIDAR EMAIL -->
                if( 
                    $.trim( $("#txtEmail").val() ) != "" 
                    &&
                    validaEmail( $.trim( $("#txtEmail").val() ) ) 
                   ){     
                    $("#divTxtEmail").removeClass("has-error").addClass("has-success");
                }else{
                    $("#divTxtEmail").removeClass("has-success").addClass("has-error");
                    temErro = true;
                }
                <!-- VALIDAR MENSAGEM -->
                if( $.trim( $("#txtMensagem").val() ) == "" ){ 
                    $("#divTxtMensagem").removeClass("has-success").addClass("has-error");
                    temErro = true;
                }else{
                    $("#divTxtMensagem").removeClass("has-error").addClass("has-success");
                }
                
                if(temErro == false){
                    
                    $.ajax({
                        type: "POST",
                        url: "valida-contato.html",
                         data : $("#formulario-contato").serializeArray(),
                        beforeSend: function(){
                            $("#btn-enviar-contato").attr('disabled', 'disabled');
                        },
                        success: function(response) {
                            alert(response);
                        },
                        error: function(){
                            alert("erro ao postar formulário!");
                        },
                        complete: function(){
                            $("#btn-enviar-contato").removeAttr('disabled', 'disabled');
                        }
                    });
                }
            }
        );
        
        
        
        $("#btn-entrar").click(
            function(){
                
                var temErro = false;
                
                <!-- VALIDAR EMAIL -->
                if( 
                    $.trim( $("#email-login").val() ) != "" 
                    &&
                    validaEmail( $.trim( $("#email-login").val() ) ) 
                   ){     
                    $("#divEmail-login").removeClass("has-error").addClass("has-success");
                }else{
                    $("#divEmail-login").removeClass("has-success").addClass("has-error");
                    temErro = true;
                }
                
                <!-- VALIDAR SENHA -->
                if( $.trim( $("#senha-login").val() ) == "" ){      <!-- trim(pra evitar espaço ser validado )-->                          
                    $("#divSenha-login").removeClass("has-success").addClass("has-error");
                    temErro = true;
                }else{
                    $("#divSenha-login").removeClass("has-error").addClass("has-success");
                }
                
                if( temErro == false){
                    $.ajax({
                        type: "POST",
                        url: "valida-login.php",
                         data : $("#form-login").serializeArray(),
                        beforeSend: function(){
                            $("#btn-entrar").attr('disabled', 'disabled');
                        },
                        success: function(response) {
                            alert("ok");                             
                        },
                        error: function(){
                            alert("erro ao postar formulário!");
                        },
                        complete: function(){
                            $("#btn-entrar").removeAttr('disabled', 'disabled');
                        }
                    });
                }else{
                    alert("Email ou senha nao preenchidos.");
                }
               
            }
        );
        
});